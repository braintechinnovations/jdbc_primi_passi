package corso.lez13.jsbc.separazione;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.mysql.cj.jdbc.MysqlDataSource;

public class GestorePersona {

	/**
	 * Stampa tutte le persone che gli passo sotto forma di ArrayList
	 * @param varArray
	 */
	public void stampaElenco(ArrayList<Persona> varArray) {
		for(int i=0; i<varArray.size(); i++) {
			Persona temp = varArray.get(i);
			System.out.println(temp.toString());
		}
	}
	
	/**
	 * Recupera tutte le persone sul DB
	 * @return
	 */
	public ArrayList<Persona> cercaTutti(){
		
		ArrayList<Persona> elenco = new ArrayList<Persona>();

		try {
			
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("localhost");						//Indirizzo del server MySql
			dataSource.setPort(3306);
			dataSource.setUser("root");
			dataSource.setPassword("toor");
			dataSource.setDatabaseName("rubrica_uno");
			dataSource.setUseSSL(false);
			dataSource.setServerTimezone("UTC");
			
			Connection conn = dataSource.getConnection();
			System.out.println("Sono connesso! :D");
			
			// -------------------------------- SELECT -----------------------------------------
			
			String selectQuery = "SELECT personaId, nome, cognome, codice_fiscale FROM persona";
			PreparedStatement ps = conn.prepareStatement(selectQuery);
		
			ResultSet result = ps.executeQuery();
			
			while(result.next()) {							//Estraggo le informazioni sulle persone dal Result Set
				Persona temp = new Persona();
				temp.setPerId( result.getInt(1) );
				temp.setPerNome( result.getString(2) );
				temp.setPerCognome( result.getString(3) );
				temp.setPerCF( result.getString(4) );
				
				elenco.add(temp);
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return elenco;
	}
	
	//TODO: Inserisci persona
	
	//TODO: Elimina persona
	
	//TODO: Modifica persona
	
	//TODO: Cerca per ID
}
	
