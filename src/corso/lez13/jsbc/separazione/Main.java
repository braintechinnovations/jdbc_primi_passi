package corso.lez13.jsbc.separazione;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		GestorePersona ges = new GestorePersona();
		
		ArrayList<Persona> elenco_completo = ges.cercaTutti();
		ges.stampaElenco(elenco_completo);
		
	}

}
