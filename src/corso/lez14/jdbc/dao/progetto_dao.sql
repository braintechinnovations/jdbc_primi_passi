DROP DATABASE IF EXISTS carte_fedelta;
CREATE DATABASE carte_fedelta;
USE carte_fedelta;

CREATE TABLE persona (
	personaId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(250),
    cognome VARCHAR(250),
    codice_fiscale VARCHAR(16) UNIQUE NOT NULL
);

CREATE TABLE carta_fedelta(
	cartaId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    negozio VARCHAR(250),
    numero_carta VARCHAR(200),
    personaRif INTEGER,
    FOREIGN KEY (personaRif) REFERENCES persona(personaId) ON DELETE SET NULL
);