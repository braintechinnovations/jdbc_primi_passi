package corso.lez14.jdbc.dao;

import java.util.ArrayList;
import java.util.List;

import corso.lez14.jdbc.dao.models.Carta;
import corso.lez14.jdbc.dao.models.CartaDAO;
import corso.lez14.jdbc.dao.models.Persona;
import corso.lez14.jdbc.dao.models.PersonaDAO;

public class Main {

	public static void main(String[] args) {

		PersonaDAO perDao = new PersonaDAO();
		
		//INSERIMENTO
//		Persona gio = new Persona("Giovanni", "Pace", "PCAGNN");
//		perDao.insert(gio);
//		if(gio.getId() > 0) {
//			System.out.println("Inserimento effettuato con successo");
//		}
//		else {
//			System.out.println("Errore di inserimento");
//		}
//		
//		Persona mar = new Persona("Mario", "Rossi", "MRRRSS");
//		perDao.insert(mar);
//		if(mar.getId() > 0) {
//			System.out.println("Inserimento effettuato con successo");
//		}
//		else {
//			System.out.println("Errore di inserimento");
//		}
		
		//RICERCA PER ID e MODIFICA
//		Persona ricercato = perDao.findById(2);
//		System.out.println(ricercato.toString());
//		
//		ricercato.setNome("Saul");
//		ricercato.setCognome("Goodman");
//		ricercato.setCod_fis("SULGDM");
//		
//		if(perDao.update(ricercato)) {
//			System.out.println("Modifica effettuata con successo, ecco i dettagli:");
//			System.out.println(ricercato.toString());
//		}
//		else {
//			System.out.println("Errore di esecuzione");
//		}
		
//		ArrayList<Persona> elenco = (ArrayList<Persona>) perDao.findAll(); //Il cast � necessario perch� la List si trasforma in ArrayList solo al Runtime
//		List<Persona> elenco = perDao.findAll();
//		
//		//Non va nel DAO perch� non � una interazione con il DB!
//		for(int i=0; i<elenco.size(); i++) {
//			Persona temp = elenco.get(i);
//			System.out.println(temp.toString());
//		}
		
		//-------------------------------------------------------------------------------------

		CartaDAO carDao = new CartaDAO();

		//INSERIMENTO
//		Persona gio = perDao.findById(2);
//		
//		Carta car = new Carta();
//		car.setNumero("AB123456");
//		car.setNegozio("CONAD");
//		car.setRiferimento(gio);
//		
//		carDao.insert(car);
//		
//		if(car.getId() > 0) {
//			System.out.println("Carta inserita con successo");
//		}
//		else {
//			System.out.println("Errore di inserimento");
//		}
		
		Carta conad = carDao.findById(1);
		System.out.println(conad);
		
	}

}
